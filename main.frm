**************************************************************************
*	Welcome to ECO, Form code accompanying the paper:
*	"Efficient Hilbert Series for Effective Theories",
*	Authors: Coenraad B. Marinissen, Rudi Rahn, and Wouter J. Waalewijn
**************************************************************************
#-
Off statistics;
#include- header_files/declare.h
#include- header_files/HilbertSeries.h
#include- header_files/addField.h

**************************************************************************
*	- As an example, the Standard Model has been worked out. 
*	- All Eq./Tab. numbers in the comments refer to the paper. 
**************************************************************************
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Specify mass dimension
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define massDim "15"

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Turning on (1) or off (0) IBP and EOM relations
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define EOM "1"
#define IBP "1"

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Number of Fermion Operators (equal to 1 by default)			
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Symbol nf;
#define numFermGen "nf"

**************************************************************************
*	Adding the fields	
*	- Declare symbols for particles/fields
* 	- Protected symbols: x,y,y1,y2,z1,z2,mass,... (see declare.h file)
*	- Call procedures to add the different spins (see Table II and V 
*		for the different options and their input). The procedures 
*		are defined in the header file addField.h.
*	- Note: complex conjugate/anti particles need to be added as 
*		independent building blocks. 			
**************************************************************************
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Adding scalars
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Symbols h,hd;
#call addScalar(h,1,2,3)
#call addScalar(hd,1,2,-3)

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Adding Fermions + additional U(1) for e.g. operators conserving 
*	Baryon number
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Symbols q,qd,u,ud,d,dd,l,ld,e,ed;

#call addLHFermion(q,3,2,1)
#call addRHFermion(qd,3B,2,-1)

#call addRHFermion(u,3,1,4)
#call addLHFermion(ud,3B,1,-4)

#call addRHFermion(d,3,1,-2)
#call addLHFermion(dd,3B,1,2)

#call addLHFermion(l,1,2,-3)
#call addRHFermion(ld,1,2,3)

#call addRHFermion(e,1,1,-6)
#call addLHFermion(ed,1,1,6)

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Adding field strengths
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Symbols B,W,G;
#call addFieldStrength(B,1,1,0)
#call addFieldStrength(W,1,3,0)
#call addFieldStrength(G,8,1,0)

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Adding gravity/Weyl tensor (uncomment these for GRSMEFT) 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Symbol C;
*#call addGravity(C,1,1,0)


**************************************************************************
*	Computing the Hilbert series at mass dimension `massDim'
*	- Declare symbol for derivative
*	- Call HilbertSeries with symbol for derivative as input. 
*	- Output: Local expression 'Hilbert' which is a polynomial in the 
*		above declared symbols representing the operators basis. 
*	- Note: for basis without derivatives: #call Hilbertseries(0)
**************************************************************************
Symbol p;
#call HilbertSeries(p)
Print +s;
.sort

Global SM15 = Hilbert;
.store
Save SM15.sav;
.sort

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Counting number of operators
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#call counting

.end
