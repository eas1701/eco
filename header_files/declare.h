**************************************************************************
*	Header file with all declerations
*	We need to declare variables, symbols and CFunctions as placeholders
*	during the expansion in the HilbertSeries procedure, s.t. the 
*	expansion doesn't blow up. We need these placeholders for the 
*	different:
*	- Characters
*	- Fields
*	Below we also comment on their nomenclature as well as in which
*	file they are used.  
**************************************************************************

**************************************************************************
*	General declarations 
*	- Dummy symbols for use in sums and id statements
*	- Dummy functions for id statements
**************************************************************************
Symbol n,m,i;
CFunctions field,f,fill;

**************************************************************************
*	Characters: 
*	- CFunctions are declared as follows:
*		char'Group'rep'Representation'
*	with 'Group' one of Lorentz, SU(3), SU(2) or U(1) symmetry groups
*	and 'Representation' the representation under that group (see
*	also Table V).  
*	- Symbols: variables for the characters (same as in Table V). 
*	- Usage: in addField.h where the CFunctions are taken during the 
*	storage of the different fields the user adds to the Hilbert Series. 
*	And in HilbertSeries.h where these CFunctions/characters are 
*	expanded in their actual form in the character variables defined 
*	here as symbols. 
**************************************************************************
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Lorentz
*	Representations:
*	- LH (1/2,0)
*	- RH (0,1/2)
*	- Derivative (1/2,1/2)
*	- Spin1 (1,0)+(0,1)
*	- Spin2 (2,0)+(0,2)
*	- Spin2Right (0,2)
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Symbols y1,y2;
CFunction charLorentzLH,charLorentzRH,charLorentzDerivative;
CFunction charLorentzSpin1,spin2,spin2Right,spin32,spin32Right;

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	SU(3)
*	Representations:
*	- 1 (trivial)
*	- 3 (fundamental)
*	- 3B (3 bar/anti fundamental)
*	- 8 (adjoint)
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Symbols z1,z2;
CFunction charSU3rep1,charSU3rep3,charSU3rep3B,charSU3rep8;

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	SU(2)
*	Representations:
*	- 1 (trivial)
*	- 2 (fundamental)
*	- 3 (adjoint)
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Symbols y;
CFunction charSU2rep1,charSU2rep2,charSU2rep3;

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	U(1) symmetry group(s)
*	Need autodeclare for variable for x as we can have multiple
* 	U(1) symmetry groups
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define numberOfU1groups "0"
Autodeclare symbol x;
CFunction Charge;


**************************************************************************
*	Fields:
*	- CFunctions are of the following form:
*		`Field' and `Field'`Group'`Representations'
*	with `Field' the different spin types and `Group' and 
*	`Representation' as above.
*	- Variables: set to 1 when a field of that type has been added
*		by the user. The HilbertSeries procedure uses these to
*		check if it needs to expand the PE's for that field type and
*		if it needs to insert representations for that field type. 
*	- Usage: CFunctions are expanded in HilberSeries.prc
*	in all fields of that type added by the user. They are expanded
*	just before the residues of the corresponding `Group' are taken. 
**************************************************************************
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Scalars
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define DEFScalar "0"
CFunction Scalar,ScalarSU21,ScalarSU22,ScalarSU23;

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Dirac, left and right handed fermions
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define DEFFermion "0"
CFunctions charFermion,charFermionEOM;

#define DEFDiracFermion "0"
CFunctions DiracFermion, DiracFermionSU21, DiracFermionSU22, DiracFermionSU23;

#define DEFLHFermion "0"
CFunction LHFermion, LHFermionSU21, LHFermionSU22, LHFermionSU23;

#define DEFRHFermion "0"
CFunction RHFermion, RHFermionSU21, RHFermionSU22, RHFermionSU23;


*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Field strengths
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define DEFFieldStrength "0"
CFunction FieldStrength;
CFunction FieldStrengthSU21, FieldStrengthSU22, FieldStrengthSU23;


*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Gravity/Weyl tensor
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define DEFGravity "0"
CFunction Gravity;
CFunction GravitySU21,GravitySU22,GravitySU23;
.global
