#procedure counting

**************************************************************************
*	This procedure counts the number of operators in the Local 
*	expression Hilbert
*	- First make a copy of the expression in HilbertCounting
*	- drop all symbols in this expression
*	- store the number of operators in the dollar variable `$number'
*	- Print the result together with the specified mass dimension
*	- Drop the expression HilbertCounting as it became obsolete. 
**************************************************************************
	Local HilbertCounting = Hilbert;
	.sort
	
	Hide Hilbert;
	dropsymbols;
	.sort

	#$number = HilbertCounting;
	#write "Number of operators at mass dimension `massDim' is `$number'."
	.sort
	
	Drop HilbertCounting;
	.sort

	Unhide Hilbert;
	.sort
#endprocedure


#procedure expandPE(Field)
**************************************************************************
*	Procedure that can expand the PE efficiently for the different
*	`Field' types. The output is a local expression `Field'PE which is
*	the expanded PE up to mass dimension `massDim' for that field type. 
**************************************************************************
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Some comments about efficiency:
*	- First expand argument of Plethystic exponential for the different
*	fields in a Local expression `Field'ArgumentPE up to mass `massDim'.
*	Note that when 'EOM' is 0, we get the argument without EOM subtracted.
*	- Sort the expression with mass outside the brackets: 
*	Use Brackets+ for a speed up: form saves the bracket information 
*	together with an index s.t. when it needs to search for a certain 
*	bracket, it knows where to look (instead of going through the whole
*	expression). 
*	- Hide the expression together with bracket information, s.t. it is
*	not an active expression anymore, but can be used on the RHS of id. 
*	- Expand the exponential up to order 'massDim' in Local expression
*	`PE'. Here f is a placeholder for the Argument. 
*	- Do a loop to replace f by the argument, without getting terms of 
*	higher mass dimensions than `massDim'. 
*	- Replace f by the argument once. Without once, the expression blows
*	up really fast as form has to compute many terms to a potential high
*	power. 
*	Furthermore, the argument doesn't need to be inserted up to `massDim'
*	so the sum goes only up to a the order needed. For the moment a 
*	placeholder field(i) is inserted because some terms will
*	drop out before the argument is inserted and expanded. 
*	- Then count powers of f and mass, when this is higher than `massDim'
*	it won't contribute to the basis at mass dimension 'massDim', so we
*	discard that term. 
*	- Check if there are still f's, when there are: need to loop once more.
*	- Now Form replaces the placeholder field by the argument and let Form
*	sort everything before we loop once more. 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#switch `Field'
	#case Scalar
		Local ScalarArgumentPE = sum_(i,1,'massDim',mass^(2*i)*Scalar(i)*(1-'EOM'*'p'^(2*i)*mass^(2*2*i))*Momentum(i)/i);
	#break
	#case FieldStrength
		Local FieldStrengthArgumentPE = sum_(i,1,'massDim',mass^(2*2*i)*FieldStrength(i)*(charLorentzSpin1(i)-'EOM'*2*'p'^i*mass^(2*i)*charLorentzDerivative(i)+'EOM'*2*'p'^(2*i)*mass^(2*2*i))*Momentum(i)/i);
	#break	
	#case Fermion
		Local FermionArgumentPE = sum_(i,1,'massDim','numFermGen'*mass^(3*i)*(-1)^(i+1)*(charFermion(i) - 'EOM'*'p'^i*mass^(2*i)*charFermionEOM(i))*Momentum(i)/i);
	#break
	#case Gravity
		Local GravityArgumentPE = sum_(i,1,'massDim',mass^(2*2*i)*Gravity(i)*(spin2(i)-'EOM'*'p'^i*mass^(2*i)*spin32(i)+'EOM'*'p'^(2*i)*mass^(2*2*i)*charLorentzSpin1(i))*Momentum(i)/i);
	#break
	#case Ibp
		Local IbpArgumentPE = sum_(i,1,'massDim', -'p'^i*mass^(2*i)*charLorentzDerivative(i)/i);
	#break
	#endswitch
	Brackets+ mass;
	.sort
	
	Hide;
	Local `Field'PE = sum_(i,0,'massDim',f^i/fac_(i));
	#do k=1,1
		id, once f*mass^n? = sum_(i,1,{2*'massDim'}-n, mass^(i)*field(i))*mass^n;
		if ( count(f,1,mass,1) > {2*'massDim'} ) Discard;
		if( match(f) ) redefine k "0";
		id field(n?) = `Field'ArgumentPE[mass^n];
		.sort
	#enddo
	
	Unhide;
	Drop `Field'ArgumentPE;
#endprocedure



#procedure HilbertSeries(p)

**************************************************************************
*
*	This procedure computes the Hilbert series at specified mass
*	mass dimension 'massDim'. The actions of the procedure are:
* 	
*	- Expansion of the generating function at mass dimension 'massDim'
*	- Computing the integral over the generating function. This is 
*		equivalent to taking residues. 
*	- All Eq./Tab. numbers in the comments refer to the paper. 
*	
*	Input:
*	- `p': the symbol declared by the user to represent the derivative
*	in the Hilbert series. It can also be equal to 1 or 0 for just 
*	counting all operators or to generate operators without 
*	derivatives respectively. 
**************************************************************************

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Test if EOM and IBP are well defined (either 1 or 0), otherwise
*	terminate. Furthermore, check if the number of fermion generations
*	is defined, otherwise set it equal to the default value 1. 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#ifndef 'numFermGen'
	#define numFermGen "1"
#endif
#if ( ('EOM'!=1) && ('EOM'!=0) )
	#write "Variable EOM cannot be equal to 'EOM', needs to be either 1 or 0"
	#terminate
#endif
#if ( ('IBP'!=1) && ('IBP'!=0) )
	#write "Variable IBP cannot be equal to 'IBP', needs to be either 1 or 0"
	#terminate
#endif

**************************************************************************
*	Declaration of a variable for the counting scheme in mass dimension
*	The variable has a cut of s.t. all terms above `massDim' are 
*	discarded. Note that we rescale the mass dimensions of all 
*	operators with a factor of 2 to make all mass dimensions integer.
**************************************************************************
Symbol mass(:{2*'massDim'});

**************************************************************************
*	Momentum Generating Function P (see Eq. (6))
*	- Because this itself is a plethystic exponential (see Eq. (3) and
*	(6)), see comments below how this can be expanded efficiently. 
*	- Furthermore, because it is used in the character for the 
*	conformal representation, we need to keep track of the power
*	of the variables in which this momentum generating function P 
*	gets expanded. 
**************************************************************************
Table Momentum(1:'massDim');

Local P = 0 + 
#do l=1,'massDim'
	+ fill('l')*sum_(i,0,'massDim',f('l')^i/fac_(i))
#enddo
;
.sort

#do k=1,1
	id, once mass^i?*f(n?) = mass^i*sum_(m,1,'massDim','p'^(n*m)*mass^(2*n*m)*charLorentzDerivative(n*m)/m);
	if( count(f,1) ) redefine k "0";
	.sort
#enddo

Brackets+ fill;
.sort

Fillexpression Momentum = P(fill);
Drop P;
.sort

**************************************************************************
*	Expanding the Plethystic Exponentials (PE/PEF), see Eq. (3), (6)
*	(8), (11), (13), (17) and (24) for the different field types up 
*	to mass dimension `massDim':
*	- Scalar									
*	- Field strength tensor						
* 	- Fermion
*	- Gravity/Weyl tensor
*	- Substracting IBP relations
**************************************************************************

#do Field={Scalar,FieldStrength,Fermion,Gravity}
	#if (`DEF`Field'')
		#call expandPE(`Field')
	#else
		Local `Field'PE = 1;
	#endif

	Brackets+ mass;
	.sort
	Hide;
#enddo

#if (`IBP')
	#call expandPE(Ibp)
#else
	Local IbpPE = 1;
#endif

Brackets+ mass;
.sort
Hide;


**************************************************************************
*	Constructing the HS at mass dimension `massDim' by multiplying the
*	above constructed Plethystic exponentials up to order `massDim'
*	- Use the dummy function f as an intermediate step s.t. tform can
*	distribute the work over the processors. 		
**************************************************************************

Local Hilbert = sum_(i,0,2*'massDim',mass^i*FieldStrengthPE[mass^i]*f(i));
.sort

id f(n?) = sum_(i,0,2*'massDim'-n,FermionPE[mass^i]*mass^i);
Brackets+ mass;
.sort

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Local Hilbert = sum_(i,0,2*'massDim',mass^i*Hilbert[mass^i]*f(i));
.sort

id f(n?) = sum_(i,0,2*'massDim'-n,GravityPE[mass^i]*mass^i);
Brackets+ mass;
.sort

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Local Hilbert = sum_(i,0,2*'massDim',mass^i*Hilbert[mass^i]*f(i));
.sort

id f(n?) = sum_(i,0,2*'massDim'-n,ScalarPE[mass^i]*mass^i);
Brackets+ mass;
.sort

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Local Hilbert = sum_(i,0,2*'massDim',Hilbert[mass^i]*f(i));
.sort

id f(n?) = IbpPE[mass^(2*'massDim'-n)];
.sort

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Drop the Local expression for the PE's as we do not need 
*	them anymore.
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Unhide;
Drop IbpPE;
Drop ScalarPE;
Drop GravityPE;
Drop FieldStrengthPE;
Drop FermionPE;
.sort


**************************************************************************
*	Computing the integral over the generating function			
*	This is equal to taking residues. 
**************************************************************************

**************************************************************************
*	Lorentz symmetry
*
*	All Lorentz characters are inserted. 
*	- Note: charcter for scalars is just equal to 1, so no need to 
*	insert this seperately.
*	- Use as basic building blocks the (1/2,0) and (0,1/2) reps via
*	their tensor products as much as possible to obtain speed up. 
*	Only for spin (2,0)+(0,2) this is not a speed up. 
*	- Insert the character once at a time with id, once s.t. Form can 
*	sort the expression as much as possible in intermediate steps. 
*	- First insert the left handed characters (with variable y1) and
*	than take the integral over y1. Some terms will be set to zero
*	before we have to insert the right handed characters (variable y2)
*	and integrate/take residues. 
**************************************************************************

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Characters for derivative
*	Use: (1/2,0) * (0,1/2) = (1/2,1/2)
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do i=1,1
	id, once charLorentzDerivative(n?) = charLorentzLH(n)*charLorentzRH(n);
	if ( count(charLorentzDerivative,1) ) redefine i "0";
	.sort
#enddo

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Characters for left handed, right handed and Dirac fermions
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do i=1,1
	id, once charFermion(n?) = 'DEFLHFermion'*charLorentzLH(n)*LHFermion(n) + 'DEFRHFermion'*charLorentzRH(n)*RHFermion(n) + 'DEFDiracFermion'*(charLorentzLH(n) + charLorentzRH(n))*DiracFermion(n);
	if ( count(charFermion,1) ) redefine i "0";
	.sort
#enddo

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Characters to subtract the EOM of the left handed, right handed 
*	and Dirac fermions
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do i=1,1
	id, once charFermionEOM(n?) = 'DEFRHFermion'*charLorentzLH(n)*RHFermion(n)+'DEFLHFermion'*charLorentzRH(n)*LHFermion(n) + 'DEFDiracFermion'*(charLorentzRH(n) + charLorentzLH(n))*DiracFermion(n);
	if ( count(charFermionEOM,1) ) redefine i "0";
	.sort
#enddo


*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	FieldStrength Lorentz characters
*	Use that we can express spin 1 in terms of spin 1/2
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do i=1,1
	id, once charLorentzSpin1(n?) = charLorentzLH(2*n) + charLorentzRH(2*n) + 2;
	if ( count(charLorentzSpin1,1) ) redefine i "0";
	.sort
#enddo

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Characters to subtract the EOM of gravity/Weyl tensor
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do i=1,1
	id, once spin32(n?) = (charLorentzLH(3*n) + charLorentzLH(n))*charLorentzRH(n) + charLorentzLH(n)*(charLorentzRH(3*n) + charLorentzRH(n));
	if ( count(spin32,1) ) redefine i "0";
	.sort
#enddo

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Filling in the explicit form of the (Left) characters
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do i=1,1
	id, once charLorentzLH(n?) = y1^n+1/y1^n;
	if ( count(charLorentzLH,1) ) redefine i "0";
	.sort
#enddo

#do i=1,1
	id, once spin2(n?) = y1^(4*n)+y1^(2*n)+1+1/y1^(2*n)+1/y1^(4*n) + spin2Right(n);
	if ( count(spin2,1) ) redefine i "0";
	.sort
#enddo


*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Residues of Lorentz group
*	See Tab. V for the Haar measure. 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Brackets+ y1;
.sort

Local Hilbert = Hilbert[1] - Hilbert[y1^(-2)];
.sort

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Filling in the explicit form of the right handed characters
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do i=1,1
	id, once charLorentzRH(n?) = y2^n+1/y2^n;
	if ( count(charLorentzRH,1) ) redefine i "0";
	.sort
#enddo

#do i=1,1
	id, once spin2Right(n?) = y2^(4*n)+y2^(2*n)+1+1/y2^(2*n)+1/y2^(4*n);
	if ( count(spin2Right,1) ) redefine i "0";
	.sort
#enddo

Brackets+ y2;
.sort

Local Hilbert = Hilbert[1] - Hilbert[y2^(-2)];
.sort




**************************************************************************
*	SU(2) symmetry					
*
*	To insert the SU(2) representations properly, we have
*	- one loop over all field types `Field'
*	- Check if that there is a field of type `Field' added, to prevent
*	unnecessary looping.
*	- one loop to replace all field types by their SU(2) representation
*	which are stored in Form tables of the form `Field'SU2reps. Do one
*	replacement at a time to sort the expression as much as possible. 
**************************************************************************

#do Field={Scalar,FieldStrength,Gravity,DiracFermion,LHFermion,RHFermion}
	#if ( `DEF`Field'' )
	#do i=1,1
		id, once `Field'(n?) = `Field'SU2reps(n);
		if ( match(`Field'(n?)) );
			redefine i "0";
		endif;
		.sort
	#enddo
	#endif
#enddo

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Filling in the characters of SU(2) 
*	Only the fundamental (2) and the adjoint (3) character need
*	to be inserted as the trivial character is just equal to 1. 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do i=1,1
	id, once charSU2rep3(n?) = charSU2rep2(2*n) + 1;
	if ( count(charSU2rep3,1) ) redefine i "0";
	.sort
#enddo

#do i=1,1
	id, once charSU2rep2(n?) = y^n + 1/y^n;
	if ( count(charSU2rep2,1) ) redefine i "0";
	.sort
#enddo


*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Residue of SU(2)
*	See Tab. V for the Haar measure. 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Brackets+ y;
.sort

Local Hilbert = Hilbert[1] - Hilbert[y^(-2)];
.sort




**************************************************************************
*	U(1) symmetry					
*
*	To insert the U(1) and SU(3) representations properly, we have 
*	- one loop over all Field types
*	- one loop over all SU(2) representations 'k'=1,2,3
*	- loop to replace every function `Field'SU2'k' by the corresponding
*	SU(2) representation of all fields of type `Field'. These 
*	representations are stored in the row 'k' of the Form table 
*	`Field'U1reps. 
**************************************************************************

#do Field={Scalar,FieldStrength,Gravity,DiracFermion,LHFermion,RHFermion}
	#if ( `DEF`Field'' )
	#do k=1,3
		#do i=1,1
			id, once `Field'SU2'k'(n?) = `Field'U1reps('k',n);
			if ( count(`Field'SU2'k',1) ) redefine i "0";
			.sort
		#enddo
	#enddo
	#endif
#enddo

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Residue of U(1), note: can have more than one U(1) symmetry group
*	See Tab. V for the Haar measure. 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do k=1,'numberOfU1groups'
	Brackets+ x'k';
	.sort

	Local Hilbert = Hilbert[1];
	.sort
#enddo



**************************************************************************
*	SU(3) symmetry
*
*	SU(3) representations are already inserted when the U(1)
*	integral was done. So can immediately replace the characters
*	of SU(3)		
**************************************************************************

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Character of fundamental representation (3)
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do i=1,1
	id, once charSU3rep3(n?) = 1/z2^n + z2^n/z1^n + z1^n;
	if ( count(charSU3rep3,1) ) redefine i "0";
	.sort
#enddo

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Character of anti fundamental representation (3B)
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do i=1,1
	id, once charSU3rep3B(n?) = 1/z1^n + z1^n/z2^n + z2^n;
	if ( count(charSU3rep3B,1) ) redefine i "0";
	.sort
#enddo

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*      Character of adjoint representation (8)
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do i=1,1
       id, once charSU3rep8(n?) = z1^n*z2^n+ z2^(2*n)/z1^n + z1^(2*n)/z2^n + 2 + z1^n/z2^(2*n) + z2^n/z1^(2*n) + 1/(z1^n*z2^n);
       if ( count(charSU3rep8,1) ) redefine i "0";
       .sort
#enddo



*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Residue of SU(3)
*	See Tab. V for the Haar measure. 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Brackets+ z1;
.sort

Local Hilbert = Hilbert[1]/z2 - Hilbert[z1^1]*z2 - Hilbert[z1^(-2)]/z2^2 + Hilbert[1]*z2^2 + Hilbert[z1^(-3)]/z2 - Hilbert[z1^(-2)]*z2;
Brackets+ z2;
.sort

Local Hilbert = Hilbert[z2^(-1)];
.sort

#endprocedure





