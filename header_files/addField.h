**************************************************************************
*	Header file with all add`Field' procedures
*
*	The procedures in this file can be used by the program user
*	to add fields to the Hilbert series. These fields can be in 
*	different representations of SU(3),SU(2) and multiple U(1)s.
*	The representations are stored in two tables for every field type. 
*	One table for all SU(2) representations and one table to store 
*	the SU(3) and all U(1) representations. These tables are used
*	in the HilbertSeries procedure to extract the representations and
*	to expand these in their actual algebraic form. 
**************************************************************************

**************************************************************************
*	Procedure that can fill the tables to store the representations
*	of all fields that are added by the user. 
*	Input of the procedure:
*	- `type': the field type, used to fill the correct table
*	- `field': the symbol given to the field by the user. 
*	- `su3': the SU(3) representation
*	- `su2': the SU(2) representation
*	- `?charge': all U(1) representations. There can be multiple of them
**************************************************************************
#procedure addField(type, field, su3, su2, ?charge)

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Test if the representations are defined properly in the Form main 
*	program. If not: terminate.
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#if ( (`su3'!=1) && (`su3'!=3) && (`su3'!=3B) && (`su3'!=8) )
	#write "SU(3) representation cannot be equal to 'su3', needs to be either 1,3,3B or 8"
	#terminate
#endif
#if ( (`su2'!=1) && (`su2'!=2) && (`su2'!=3) )
	#write "SU(2) representation cannot be equal to 'su2', needs to be either 1,2 or 3"
	#terminate
#endif

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Fill table with all SU2 representations
*	- Note: can set directly the trivial character equal to 1. 
*	- If more than one field with the same rep under SU(2) is
*	added, need to drop coefficient in order to prevent overcounting!
*	- To easily fill the table, first put everything in a Local
*	expression and fill the table with the Fillexpression command. 
*	- Drop Local expressions in the end as we do not need them
*	anymore. 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Local SU2rep = sum_(n,1,`massDim',fill(n)*`type'SU2reps(n)) + sum_(n,1,`massDim',fill(n)*charSU2rep`su2'(n)*`type'SU2`su2'(n));
id charSU2rep1(n?) = 1;
.sort

DropCoefficient;
Brackets+ fill;
.sort

Fillexpression 'type'SU2reps = SU2rep(fill);
Drop SU2rep;
.sort

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	There can be multiple U(1) representations. So we keep 
*	track of the number of U(1)'s added in the dollar variable 
*	`$numberOfCharges'. 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#$numberOfCharges = nargs_('?charge');
#if ( '$numberOfCharges' > 'numberOfU1groups' )
	#redefine numberOfU1groups "{'$numberOfCharges'}"
#endif

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Fill table with all U(1) and SU(3) representations.
*	- Row `k' of Table `type'U1reps represents SU(2) representation 
*	`k' (1,2 or 3)
*	- Put the U(1) charges first in the argument of the Charge function
*	- Set the trivial rep of SU(3) equal to 1.
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Local Field = sum_(n,1,`massDim',fill(`su2',n)*`type'U1reps(`su2',n)) + sum_(n,1,`massDim',fill(`su2',n)*`field'^n*Charge(`?charge')^n*charSU3rep`su3'(n));
id charSU3rep1(n?) = 1;

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	- Insert the characters of all the U(1) representations. Here
*	the first charge gets symbol x1, the second symbol x2 and so on. 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#do k=1,`numberOfU1groups'
	id Charge(n?,?n) = x'k'^n*Charge(?n);
#enddo
id Charge = 1;
Brackets+ fill;
.sort

Fillexpression `type'U1reps = Field(fill);
Drop Field;
.sort

#endprocedure


**************************************************************************
*	Procedure that can allocate the tables used to store the 
*	representations off the fields of type `field'.
*	Furthermore, it checks if `massDim' is in the correct range.
*	Input:
*	- `field': the type of the field, s.t. the procedure can allocate
*	the correct table for that type. 
**************************************************************************
#procedure allocTab(type)

#if ( 'massDim' < 1 )
	#write "Mass dimension cannot be equal to 'massDim'"
	#terminate
#endif

Table,zerofill 'type'SU2reps(1:'massDim');
Table,zerofill 'type'U1reps(1:3,1:'massDim');

#endprocedure


**************************************************************************
*	All procedure that can be used to add the fields of different spins
*	- Scalar
*	- Field strength
*	- Dirac, left/right handed fermions
*	- Gravity/Weyl tensor
*	
*	All these procedures check first if a field of the same kind have
*	been added yet. If not, than a Table is allocated with a call
*	of allocTab procedure. These tables are used to store all 
*	representations. Then the addField procedure is called to fill 
*	these tables. 
*	Input of the procedure:
*	- `field': the symbol given to the field by the user. 
*	- `su3': the SU(3) representation
*	- `su2': the SU(2) representation
*	- `?charge': all U(1) representations. There can be multiple of them
**************************************************************************

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Scalar
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#procedure addScalar(field, su3, su2, ?charge)

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Allocate the table if no Scalar field has been added yet
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#if( 'DEFScalar'==0 )
	#redefine DEFScalar "1"
	#call allocTab(Scalar)
#endif

*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Call the procedure that can fill te tables with the representations
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#call addField(Scalar,'field','su3','su2','?charge')

#endprocedure


*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Field Strength
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#procedure addFieldStrength(field, su3, su2, ?charge)

#if( 'DEFFieldStrength'==0 )
	#redefine DEFFieldStrength "1"
	#call allocTab(FieldStrength)
#endif

#call addField(FieldStrength,'field','su3','su2','?charge')

#endprocedure


*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Dirac Fermion
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#procedure addDiracFermion(field, su3, su2, ?charge)

#if( 'DEFDiracFermion'==0 )
	#redefine DEFFermion "1"
	#redefine DEFDiracFermion "1"
	#call allocTab(DiracFermion)
#endif

#call addField(DiracFermion,'field','su3','su2','?charge')

#endprocedure


*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Left Handed Fermion
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#procedure addLHFermion(field, su3, su2, ?charge)

#if( 'DEFLHFermion'==0 )
	#redefine DEFFermion "1"
	#redefine DEFLHFermion "1"
	#call allocTab(LHFermion)
#endif

#call addField(LHFermion,'field','su3','su2','?charge')

#endprocedure


*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Left Handed Fermion
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#procedure addRHFermion(field, su3, su2, ?charge)

#if( 'DEFRHFermion'==0 )
	#redefine DEFFermion "1"
	#redefine DEFRHFermion "1"
	#call allocTab(RHFermion)
#endif

#call addField(RHFermion,'field','su3','su2','?charge')

#endprocedure


*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*	Gravity
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#procedure addGravity(field, su3, su2, ?charge)

#if( 'DEFGravity'==0 )
	#redefine DEFGravity "1"
	#call allocTab(Gravity)
#endif

#call addField(Gravity,'field','su3','su2','?charge')

#endprocedure
